var helpOverflowApp = angular.module('helpOverflowApp', [
    'ngRoute',
    'helpOverflowControllers'
]);


helpOverflowApp.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider.when('/', {
            templateUrl: 'questions.html',
            controller: 'Controller1'
        }).when('/question', {
            templateUrl: '',
            controller: ''
        }).when('/login', {
            templateUrl: 'login.html',
            controller: 'Controller1'
        }).otherwise({
            redirectTo: '/'
        });
    }]);


var helpOverflowControllers = angular.module('helpOverflowControllers', []);

helpOverflowControllers.controller('Controller1', ['$scope', '$http',
    function($scope, $http) {
        $http.get('').success(function(data) {
            $scope.phones = data;
        });

        $scope.orderProp = 'age';
    }]);

helpOverflowControllers.controller('Controller2', ['$scope', '$routeParams',
    function($scope, $routeParams) {
        $scope.phoneId = $routeParams.phoneId;
    }]);