package com.helpoverflow.rest;

import com.helpoverflow.domain.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by michael on 14/11/15.
 */
@RestController
public class MainController {

    private static User michael_User = new User("michael", "pass123", 13);
    private static User rafael_User = new User("rafael_User", "pass123", 30);
    private static User martin_User = new User("martin_User", "pass123", 27);

    private final static List<User> users = new ArrayList<>();
    static {
        users.add(michael_User);
        users.add(rafael_User);
        users.add(martin_User);
    }

    private static Tag publicTransport_Tag = new Tag("public-transport");
    private static Tag activity_Tag = new Tag("activity");
    private static Tag food_Tag = new Tag("food");

    private static Answer answer1_1 = new Answer(1L, martin_User, "At a train ticket automat", "At a train ticket automat", Language.ENGLISH);
    private static Answer answer1_2 = new Answer(1L, rafael_User, "Wenn es Sonderzüge gibt, dann unbedingt ausnutzen anstatt für diese Strecke unnötig Geld zu bezahlen. Ab ca 17h können wir Bescheid geben, ob es welche geben wird oder nicht. ",
            "If there are extra trains you should use it instead of paying money for it. At 17 o'clock we can tell you if there will be any.", Language.GERMAN);
    private static Answer answer2_1 = new Answer(1L, martin_User, "At the Donauinsel", "At the Donauinsel", Language.ENGLISH);
    private static Answer answer4_1 = new Answer(1L, martin_User, "Aufgrund der Hauptsaison sind alle Hotelzimmer ausgebucht. Falls Sie doch Hotelzimmer finden, dann müssen sie einen Pass haben. ",
            "Because of the main season all hotel rooms are taken. If you find a free hotel room, you need a passport", Language.GERMAN);

    private static Question question1 = new Question(michael_User, "Ich möchte nach Berlin fahren und Tickets kaufen. Was soll ich tun?",
            "I want to go to Berlin and buy train tickets. What can I do?", Arrays.asList(publicTransport_Tag), 17, 0,
            48.201796, 16.369730, Language.GERMAN,
            Arrays.asList(answer1_1, answer1_2));
    private static Question question2 = new Question(rafael_User, "Where can I play with my children outside?",
            "Where can I play with my children outside?", Arrays.asList(activity_Tag), 17, 0, 48.197665, 16.354062, Language.ENGLISH,
            Arrays.asList(answer2_1));
    private static Question question3 = new Question(rafael_User, "Where can I buy milk at night?",
            "Where can I buy milk at night?", Arrays.asList(food_Tag), 17, 0, 48.301654, 14.297554, Language.ENGLISH,
            Arrays.asList());
    private static Question question4 = new Question(rafael_User, "Ich möchte mir ein Hotelzimmer buchen. Geht das?",
            "I want to book a hotel room. Does it work?", Arrays.asList(food_Tag), 17, 0, 48.301654, 14.297554, Language.ENGLISH,
            Arrays.asList(answer4_1));

    private final static List<Question> questions = new ArrayList<>();

    private final static Map<Long, Question> questions_Storage = new HashMap<>();

    static {
        addQuestion(question1);
        addQuestion(question2);
        addQuestion(question3);
        addQuestion(question4);
    }


    @RequestMapping(value = "/questions", method = RequestMethod.GET)
    public ResponseEntity<List<String>> questions_GET() {
        return new ResponseEntity(questions, HttpStatus.OK);
    }

    @RequestMapping(value = "/question/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> question_GET_id(@PathVariable Long id) {
        if (!questions_Storage.containsKey(id)) {
            return new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity(questions_Storage.get(id), HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/questions/query", method = RequestMethod.GET)
    public ResponseEntity<List<String>> question_GET_searchstring(@RequestParam(value = "q") String query) {
        String[] queryWords = query.split(" ");
        ArrayList<String> relevantWords = new ArrayList<>();
        for (String word : queryWords) {
            if (word.length() >= 4) {
                relevantWords.add(word);
            }
        }

        List<Question> result = new ArrayList<>();
        for (Question q : questions) {
            for (String word : relevantWords) {
                if (q.getNormalizedQuestion().contains(word)) {
                    result.add(q);
                    break;
                }
            }
        }
        return new ResponseEntity(result, HttpStatus.OK);
    }

    @RequestMapping(value = "/question", method = RequestMethod.POST)
    public ResponseEntity<Question> question_POST(@RequestBody Question question) {
        if (question.getQuestion() == null || "".equals(question.getQuestion()) || question.getLongitude() == null || question.getLatitude() == null || question.getLanguage() == null) {
            new ResponseEntity<>("", HttpStatus.BAD_REQUEST);
        }
        questions.add(question);
        return new ResponseEntity<Question>(question, HttpStatus.OK);
    }

    private static long nextQuestionId = 1;
    private static void addQuestion(Question question) {
        question.setId(nextQuestionId);
        questions.add(question);
        questions_Storage.put(question.getId(), question);
        nextQuestionId++;
    }

    private static void removeQuestion(Question question) {
        Long id = question.getId();
        if (questions_Storage.containsKey(id)) {
            Question removed = questions_Storage.remove(id);
            questions.remove(removed);
        }
    }

}
