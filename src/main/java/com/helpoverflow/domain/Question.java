package com.helpoverflow.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by michael on 14/11/15.
 */
public class Question implements Serializable {

    private Long id;

    private User user;
    private String question;
    private String normalizedQuestion;
    private List<Tag> tags;

    private Integer upVotes;
    private Integer downVotes;

    private Double longitude;
    private Double latitude;

    private Language language;

    private List<Answer> answers = new ArrayList<>();

    public Question(User user, String question, String normalizedQuestion, List<Tag> tags, Integer upVotes, Integer downVotes, Double longitude, Double latitude, Language language, Collection<Answer> answers) {
        this.user = user;
        this.question = question;
        this.normalizedQuestion = normalizedQuestion;
        this.tags = tags;
        this.upVotes = upVotes;
        this.downVotes = downVotes;
        this.longitude = longitude;
        this.latitude = latitude;
        this.language = language;
        this.answers = new ArrayList<>(answers);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public Integer getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(Integer upVotes) {
        this.upVotes = upVotes;
    }

    public Integer getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(Integer downVotes) {
        this.downVotes = downVotes;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addAnswer(Answer answer) {
        answers.add(answer);
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public String getNormalizedQuestion() {
        return normalizedQuestion;
    }

    public void setNormalizedQuestion(String normalizedQuestion) {
        this.normalizedQuestion = normalizedQuestion;
    }
}
