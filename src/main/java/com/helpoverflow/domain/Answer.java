package com.helpoverflow.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by michael on 14/11/15.
 */
public class Answer implements Serializable {

    private Long id;
    private User user;
    private String text;
    private String normalizedText;
    private Language language;

    private Map<Language, Integer> upVotes = new HashMap<>();
    private Map<Language, Integer> downVotes = new HashMap<>();

    public Answer(Long id, User user, String text, String normalizedText, Language language) {
        this.id = id;
        this.user = user;
        this.text = text;
        this.normalizedText = normalizedText;
        this.language = language;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void addUpVote(Language lang) {
        if (!upVotes.containsKey(lang)) {
            upVotes.put(lang, 0);
        }
        Integer was = upVotes.get(lang);
        upVotes.put(lang, was + 1);
    }

    public Integer getUpVotes(Language lang) {
        if (!upVotes.containsKey(lang)) {
            return 0;
        } else {
            return upVotes.get(lang);
        }
    }

    public void addDownVote(Language lang) {
        if (!downVotes.containsKey(lang)) {
            downVotes.put(lang, 0);
        }
        Integer was = downVotes.get(lang);
        downVotes.put(lang, was + 1);
    }

    public Integer getDownVotes(Language lang) {
        if (!downVotes.containsKey(lang)) {
            return 0;
        } else {
            return downVotes.get(lang);
        }
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getNormalizedText() {
        return normalizedText;
    }

    public void setNormalizedText(String normalizedText) {
        this.normalizedText = normalizedText;
    }

}
