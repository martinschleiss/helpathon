package com.helpoverflow.domain;

import java.io.Serializable;

/**
 * Created by michael on 14/11/15.
 */
public enum  Language implements Serializable {
    GERMAN,
    ENGLISH,
    ARABIC,
    PERSIAN
}
