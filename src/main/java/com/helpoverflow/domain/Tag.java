package com.helpoverflow.domain;

import java.io.Serializable;

/**
 * Created by michael on 14/11/15.
 */
public class Tag implements Serializable {

    private String name;

    public Tag() {
    }

    public Tag(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
