package com.helpoverflow.domain;

import java.io.Serializable;

/**
 * Created by michael on 14/11/15.
 */
public class User implements Serializable {

    private String username;
    private String password;

    private Integer rewards;


    public User(String username, String password, Integer rewards) {
        this.username = username;
        this.password = password;
        this.rewards = rewards;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getRewards() {
        return rewards;
    }

    public void setRewards(Integer rewards) {
        this.rewards = rewards;
    }
}
